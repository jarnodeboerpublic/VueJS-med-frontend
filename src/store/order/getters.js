import { unixToMoment, dateTimeHelper, dateTimeFormats } from '@/helpers/dateTimeHelper';
import orderStatus from '@/helpers/orderStatusHelper';

const setTimeDifference = item => {
  if (item && item.deliveryDateTimeWindow && item.deliveryDateTimeWindowEnd) {
    const timeDifference = `${dateTimeHelper(
      item.deliveryDateTimeWindow,
      dateTimeFormats.time,
    )} - ${dateTimeHelper(item.deliveryDateTimeWindowEnd, dateTimeFormats.time)}`;

    return { ...item, timeDifference };
  }
  return item;
};

export default {
  orderById: state => id => {
    const order = state.orders.find(x => x.orderNumber === id);

    return setTimeDifference(order);
  },
  nextOrder: state => {
    const nextOrder = state.orders.reduce((acc, cur) => {
      if (!acc) return orderStatus[cur.status].order < 4 ? cur : acc;
      if (orderStatus[cur.status].order >= 4) return acc;

      // prettier-ignore
      // eslint-disable-next-line max-len
      return unixToMoment(cur.deliveryDateTimeWindow).isBefore(unixToMoment(acc.deliveryDateTimeWindow))
        || (cur.deliveryDateTimeWindow === acc.deliveryDateTimeWindow)
        ? cur
        : acc;
    }, null);

    return setTimeDifference(nextOrder);
  },
  orders: state => {
    return state.orders.map(item => setTimeDifference(item));
  },
};
