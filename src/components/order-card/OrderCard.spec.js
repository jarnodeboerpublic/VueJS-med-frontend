import mount from '@/test-utils';
import OrderCard from './OrderCard';
import orderMock from '@/mocks/order.mock';

describe('OrderCard component', () => {
  it('should render a OrderCard component', () => {
    const wrapper = mount(OrderCard, {
      propsData: { item: orderMock[0] },
    });

    expect(wrapper).toMatchSnapshot();
  });
});
