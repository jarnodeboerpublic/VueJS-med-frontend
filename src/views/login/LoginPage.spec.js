import { shallowMount } from '@vue/test-utils';
import LoginPage from '@/views/login/LoginPage';

describe('LoginPage', () => {
  it('should render a login page', () => {
    const wrapper = shallowMount(LoginPage);

    expect(wrapper).toMatchSnapshot();
  });
});
