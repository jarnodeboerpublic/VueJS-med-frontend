import mount from '@/test-utils';
import StatusChip from './StatusChip';

describe('StatusChip component', () => {
  it('should render a StatusChip component', () => {
    const wrapper = mount(StatusChip, {
      propsData: { status: 'INBEHANDELING' },
    });

    expect(wrapper).toMatchSnapshot();
  });
});
