import mount from '@/test-utils';
import LoginForm from './LoginForm';

describe('LoginForm component', () => {
  let wrapper;
  const login = jest.fn(() => Promise.resolve());

  beforeEach(() => {
    wrapper = mount(
      LoginForm,
      {
        sync: false,
      },
      {
        modules: {
          auth: {
            actions: {
              login,
            },
          },
        },
      },
    );
  });

  it('should render a LoginForm component', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('should validate the email input', () => {
    const validators = wrapper.vm.$data.emailRules;

    expect(validators[0]()).toEqual('Het emailadres is een verplicht veld.');
    expect(validators[0]('abc@bcd.com')).toBe(true);

    const errorMessage = 'Het emailadres is ongeldig.';
    expect(validators[1]()).toEqual(errorMessage);
    expect(validators[1]('@abc.com')).toEqual(errorMessage);
    expect(validators[1]('abc@cd')).toEqual(errorMessage);
    expect(validators[1]('abc@.com')).toEqual(errorMessage);
    expect(validators[1]('abc@cd.c')).toEqual(errorMessage);

    expect(validators[1]('abc@bcd.com')).toBe(true);
  });

  it('should change email color when invalid input', () => {
    wrapper.vm.setEmailValidFromError(true);

    expect(wrapper.vm.isEmailValid).toBe(false);
    expect(wrapper.vm.emailColor).toEqual('error');
  });

  it('should change email color when valid input', () => {
    wrapper.vm.setEmailValidFromError(false);

    expect(wrapper.vm.isEmailValid).toBe(true);
    expect(wrapper.vm.emailColor).toEqual('success');
  });

  it('should validate the password input', () => {
    const validators = wrapper.vm.$data.passwordRules;

    expect(validators[0]()).toEqual('Het wachtwoord is een verplicht veld.');
    expect(validators[0]('a')).toBe(true);
  });

  it('should change password color when invalid input', () => {
    wrapper.vm.setPasswordValidFromError(true);

    expect(wrapper.vm.isPasswordValid).toBe(false);
    expect(wrapper.vm.passwordColor).toEqual('error');
  });

  it('should change password color when valid input', () => {
    wrapper.vm.setPasswordValidFromError(false);

    expect(wrapper.vm.isPasswordValid).toBe(true);
    expect(wrapper.vm.passwordColor).toEqual('success');
  });

  it('should enable the button when all input is valid', () => {
    wrapper.setData({
      email: 'abc@def.com',
      password: 'a',
      valid: true,
    });

    wrapper.vm.$nextTick(() => {
      expect(wrapper.find('button').element.disabled).toBe(false);
    });
  });

  it('should flip the password visiblity', () => {
    wrapper.find('.mdi-eye-outline').trigger('click');

    wrapper.vm.$nextTick(() => {
      expect(wrapper).toMatchSnapshot();
    });
  });

  it('should submit the form', () => {
    wrapper.setData({
      email: 'abc@def.com',
      password: 'a',
      valid: true,
    });

    wrapper.vm.$nextTick(() => {
      wrapper.find('form').trigger('submit');

      expect(login).toHaveBeenCalled();
    });
  });
});
