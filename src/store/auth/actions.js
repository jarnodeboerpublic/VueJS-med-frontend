import axios from 'axios';
import { fakeLoginAllowed, fakeLogoutAllowed } from '@/mocks/backend.mock';

export default {
  login: async ({ commit, dispatch, rootState }, payload) => {
    // Todo Remove this login
    if (fakeLoginAllowed({ commit, rootState }, payload)) return;

    dispatch('setLoading', null, { root: true });

    await axios
      .post('/api/auth/login', payload, {
        withCredentials: true,
      })
      .then(res => {
        commit('setLoggedIn', true);
        commit('setRole', res.data.role);
      })
      .catch(error => {
        if (error.response.status !== 401) {
          commit('setError', error);
        }
      })
      .finally(() => {
        commit('setPending', false, { root: true });
      });
  },
  logout: async ({ commit, rootState }) => {
    // Todo Remove this logout
    if (fakeLogoutAllowed({ commit, rootState })) return;

    await axios
      .post('/api/auth/logout', {
        withCredentials: true,
      })
      .then(() => {
        commit('setLoggedIn', false);
        commit('setRole', null);
        commit('order/clearOrders');
        commit('setError', null);
      })
      .catch(error => {
        commit('setError', error);
      });
  },
};
