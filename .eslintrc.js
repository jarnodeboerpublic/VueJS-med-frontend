module.exports = {
  root: true,
  env: {
    browser: true,
    jest: true,
    node: true,
    es6: true,
  },
  extends: ['plugin:vue/essential', '@vue/airbnb'],
  rules: {
    'arrow-parens': 'off',
    'arrow-body-style': 'off',
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'import/extensions': ['off', 'never'],
    'linebreak-style': 0,
  },
  parserOptions: {
    parser: 'babel-eslint',
    ecmaVersion: 6,
  },
  overrides: [
    {
      files: ['**/__snapshots__/*.{j,t}s?(x)'],
      env: {
        jest: true,
      },
    },
  ],
};
