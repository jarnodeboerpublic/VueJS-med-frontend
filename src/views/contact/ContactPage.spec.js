import { shallowMount } from '@vue/test-utils';
import ContactPage from './ContactPage';

describe('Contact component', () => {
  it('should render a Contact page', () => {
    const wrapper = shallowMount(ContactPage);

    expect(wrapper).toMatchSnapshot();
  });
});
