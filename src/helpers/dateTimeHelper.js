import moment from 'moment';

export const dateTimeFormats = {
  date: 'DD-MM-YYYY',
  time: 'HH:mm',
  dateTime: 'DD-MM-YYYY - HH:mm',
};

export const unixToMoment = dateTime => {
  if (typeof dateTime === 'number') {
    return moment.unix(dateTime).utc();
  }
  return null;
};

export const dateTimeHelper = (
  dateTime,
  outputFormat = dateTimeFormats.dateTime,
  dateTimeLocale = 'nl',
) => {
  if (typeof dateTime === 'number') {
    return moment(unixToMoment(dateTime))
      .local(dateTimeLocale)
      .format(outputFormat);
  }
  return 'invalid date';
};
