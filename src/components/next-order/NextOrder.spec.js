import mount from '@/test-utils';
import NextOrder from './NextOrder';
import orderMock from '@/mocks/order.mock';

describe('NextOrder component', () => {
  it('should render a NextOrder component', () => {
    const wrapper = mount(NextOrder, {
      propsData: { item: orderMock[0] },
    });

    expect(wrapper).toMatchSnapshot();
  });
});
