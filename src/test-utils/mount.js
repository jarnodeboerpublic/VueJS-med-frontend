// eslint-disable-next-line import/no-extraneous-dependencies
import { mount, createLocalVue } from '@vue/test-utils';
import Router from 'vue-router';
import Vuetify from 'vuetify';
import Vuex from 'vuex';
import createStore from './store';

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.use(Router);

const localMount = (component, compProps, storeProps) => {
  const store = createStore(storeProps);
  const vuetify = new Vuetify();
  const router = new Router();

  return mount(component, {
    stubs: ['router-link'],
    localVue,
    router,
    store,
    vuetify,
    ...compProps,
  });
};

export default localMount;
