export default {
  headers: [
    { text: 'Artikelnummer', value: 'product.id' },
    { text: 'Artikel', value: 'product.name' },
    { text: 'Aantal', value: 'amount' },
  ],
};
