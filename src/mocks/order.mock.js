import orderStatus from '../helpers/orderStatusHelper';

const status = Object.keys(orderStatus);

const clients = [
  {
    id: 123,
    name: 'Pietje',
    lastName: 'Bel',
  },
  {
    id: 124,
    name: 'Jantje',
    lastName: 'Janssen',
  },
];

const streets = ['Pad van de eeuwige fantasie', 'Straat123'];

const residences = ['WeetIkWaarHoven', 'Egmond aan Zee'];

const orderDate = 1575148902;

const orders = [...Array(30).keys()]
  .map((e, i) => i + 1)
  .map(i => ({
    orderNumber: `${123012 + i}`,
    client: clients[i % clients.length],
    deliveryStreet: streets[i % streets.length],
    deliveryStreetNumber: '1',
    deliveryResidence: residences[i % residences.length],
    postalCode: '8011 PK',
    orderDate: 1172796696,
    deliveryDateTimeWindow: orderDate + 24 * 59 * 60 * i,
    deliveryDateTimeWindowEnd: orderDate + 24 * 59 * 60 * i + 60 * 60,
    status: status[i % status.length],
    products: [
      {
        product: {
          id: '1',
          name: 'Rolstoel',
        },
        amount: 1,
      },
      {
        product: {
          id: '2',
          name: 'krukken',
        },
        amount: 2,
      },
    ],
  }));

export default orders;
