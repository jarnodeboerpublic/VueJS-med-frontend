# VueJS-med-frontend
Samen met drie andere studiegenoten hebben wij dit project gemaakt.
 
De frontend is opgezet in VueJS waarin de routing wordt gedaan met Express, Vue-router en Axios.


## Install dependencies
```
yarn
```

### Compiles and hot-reloads for development
```
 yarn serve
```

### To develop without backend
```
 yarn serve:local
```


### Compiles and minifies for production
```
yarn build
```

### Run your tests
```
yarn test
```

### Lints and fixes files
```
yarn lint
or
yarn lint --fix
```

### Styling
```
yarn format
or
yarn format --write
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
