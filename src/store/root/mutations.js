export default {
  setError: (state, payload) => {
    state.error = payload;
  },
  setPending: (state, payload) => {
    state.pending = payload;
  },
};
