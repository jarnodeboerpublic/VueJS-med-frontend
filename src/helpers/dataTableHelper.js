import orderStatus from '@/helpers/orderStatusHelper';

export const customSort = (items, [index], [isDesc]) => {
  const orderedItems = items.sort((a, b) => {
    if (index === 'status') {
      return orderStatus[a.status].order < orderStatus[b.status].order ? -1 : 1;
    }

    if (index === 'client.name') {
      return a.client.name < b.client.name ? -1 : 1;
    }

    return a[index] < b[index] ? -1 : 1;
  });

  return !isDesc ? orderedItems : orderedItems.reverse();
};

export default customSort;
