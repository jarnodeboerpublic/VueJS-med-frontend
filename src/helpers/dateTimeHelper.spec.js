import { dateTimeFormats, dateTimeHelper } from './dateTimeHelper';

describe('Should test timeHelper formater', () => {
  it('should return time when unix timstamp is passed', () => {
    expect(dateTimeHelper(1572552848, dateTimeFormats.time)).toBe('20:14');
  });

  it('should be null when date input is not a number', () => {
    expect(dateTimeHelper('1572552848', dateTimeFormats.time)).toBe('invalid date');
  });
});

describe('Should test dateHelper formater', () => {
  it('should return date when unix timstamp is passed', () => {
    expect(dateTimeHelper(1572552848, dateTimeFormats.date)).toBe('31-10-2019');
  });

  it('should be null when date input is not a number', () => {
    expect(dateTimeHelper('1572552848', dateTimeFormats.date)).toBe('invalid date');
  });
});

describe('Should test dateTimeHelper formater', () => {
  it('should return dateTime object when unix timstamp is passed', () => {
    expect(dateTimeHelper(1572552848, dateTimeFormats.dateTime)).toBe('31-10-2019 - 20:14');
  });

  it('should be null when date input is not a number', () => {
    expect(dateTimeHelper('1572552848', dateTimeFormats.dateTime)).toBe('invalid date');
  });
});
