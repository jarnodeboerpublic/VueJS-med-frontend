import Vuex from 'vuex';
import { merge, cloneDeep } from 'lodash';
import { storeConfig } from '@/store/store';
import orderMock from '@/mocks/order.mock';
import userMock from '@/mocks/user.mock';

export default extend => {
  return new Vuex.Store(
    cloneDeep(
      merge(
        storeConfig,
        {
          modules: {
            order: {
              actions: {
                getOrderList: ({ commit }) => commit('setOrders', orderMock),
              },
              getters: {
                orderById: () => id => orderMock.find(order => order.orderNumber === id),
              },
            },
            user: {
              getters: {
                getUser: ({ commit }) => commit('setUser', userMock),
              },
              actions: {
                saveUserChanges: ({ commit }, payload) => commit('setUser', payload),
              },
            },
          },
        },
        extend,
      ),
    ),
  );
};
