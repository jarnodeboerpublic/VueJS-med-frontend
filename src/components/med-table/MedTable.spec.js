import mount from '@/test-utils';
import MedTable from './MedTable';
import orderMock from '@/mocks/order.mock';

describe('MedTable component', () => {
  let wrapper;

  beforeAll(async () => {
    wrapper = mount(MedTable, {
      propsData: {
        orders: [],
        itemsPerPage: 5,
        loading: false,
        headers: [
          { text: 'Bestelling', value: 'orderNumber' },
          { text: 'Klantnaam', value: 'client.name' },
          { text: 'Adres', value: 'deliveryStreet' },
          { text: 'Plaats', value: 'deliveryResidence' },
          { text: 'Bezorgdatum', value: 'deliveryDateTimeWindow' },
          { text: 'Tijdstip', value: 'timeDifference' },
          { text: 'Status', value: 'status' },
        ],
      },
    });
    await wrapper.vm.$nextTick();
  });

  it('should render a empty MedTable', () => {
    expect(wrapper.html()).toContain('Er zijn geen bestellingen beschikbaar');
  });

  it('should render a MedTable component', () => {
    wrapper.setProps({
      orders: orderMock,
    });
    expect(wrapper).toMatchSnapshot();
  });
});
