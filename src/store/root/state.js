export default {
  error: null,
  localDev: process.env.VUE_APP_LOCALDEV === 'true',
  pending: false,
};
