// vue.config.js
module.exports = {
  // options...
  publicPath: '/',
  devServer: {
    proxy: {
      '/api': {
        target: 'http://localhost:5000',
      },
    },
  },
};
