export default {
  setLoading: ({ commit }) => {
    commit('setError', null);
    commit('setPending', true);
  },
};
