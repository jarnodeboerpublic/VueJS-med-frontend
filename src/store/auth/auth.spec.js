import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import { cloneDeep } from 'lodash';

import { storeConfig } from '../store';

Vue.use(Vuex);

jest.mock('axios');

describe('Store auth', () => {
  let store;

  beforeEach(() => {
    store = new Vuex.Store(cloneDeep(storeConfig));
  });

  it('should handle a valid login', async () => {
    axios.post.mockResolvedValue({ data: 'authorized' });

    await store.dispatch('login');

    expect(store.state.auth.loggedIn).toEqual(true);
    expect(store.state.pending).toEqual(false);
  });

  it('should handle login in dev mode', async () => {
    store.state.localDev = true;

    await store.dispatch('login', {
      email: 'admin@med.nl',
      password: 'admin',
    });

    expect(store.state.auth.loggedIn).toEqual(true);
  });

  it('should handle invalid login in dev mode', async () => {
    const error = { response: { status: 401 } };

    store.state.localDev = true;
    axios.post.mockRejectedValue(error);

    await store.dispatch('login', {
      email: 'admin@med.nl',
      password: 'foulty',
    });

    expect(store.state.auth.loggedIn).toEqual(false);
  });

  it('should handle an invalid login', async () => {
    axios.post.mockRejectedValue({ response: { status: 401 } });

    await store.dispatch('login');

    expect(store.state.auth.loggedIn).toEqual(false);
  });

  it('should handle an api error', async () => {
    const error = { response: { status: 500 } };

    axios.post.mockRejectedValue({ response: { status: 500 } });

    await store.dispatch('login');

    expect(store.state.auth.loggedIn).toEqual(false);
    expect(store.state.error).toEqual(error);
  });

  it('should logout', () => {
    store.dispatch('logout');

    expect(store.state.auth.loggedIn).toEqual(false);
    expect(store.state.order.orders).toEqual([]);
  });
});
