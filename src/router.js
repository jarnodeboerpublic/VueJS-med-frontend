import Vue from 'vue';
import Router from 'vue-router';
import DashboardPage from '@/views/dashboard/DashboardPage';
import LoginPage from '@/views/login/LoginPage';
import OrderOverview from '@/views/order/overview/OrderOverview';
import OrderDetails from '@/views/order/details/OrderDetails';
import ProfilePage from '@/views/profile/ProfilePage';
import store from '@/store/store';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'login',
      component: LoginPage,
    },
    {
      path: '/bestellingen',
      name: 'order',
      component: OrderOverview,
      meta: { requiresAuth: true },
    },
    {
      path: '/bestellingen/:id',
      name: 'orderDetails',
      component: OrderDetails,
      props: true,
      meta: { requiresAuth: true },
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: DashboardPage,
      meta: {
        requiresAuth: true,
        requiresAdmin: true,
      },
    },
    {
      path: '/profiel',
      name: 'profile',
      component: ProfilePage,
      meta: { requiresAuth: true },
    },
    {
      path: '/contact',
      name: 'contact',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/contact/ContactPage.vue'),
    },
    {
      path: '*',
      redirect: '/',
    },
  ],
});

router.beforeEach((to, from, next) => {
  const { loggedIn } = store.state.auth;
  const { isAdmin } = store.getters;
  const { requiresAuth, requiresAdmin } = to.meta;

  if (to.path === '/' && loggedIn) {
    if (isAdmin) {
      next('/dashboard');
    } else {
      next('/bestellingen');
    }
  } else if ((requiresAuth && !loggedIn) || (requiresAdmin && !isAdmin)) {
    next('/');
  } else {
    next();
  }
});

export default router;
