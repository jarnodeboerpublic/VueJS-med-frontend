import { shallowMount } from '@vue/test-utils';

import MapView from './MapView';

describe('Mapview component', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(MapView, {
      propsData: {
        postalCode: '1234AB',
      },
    });
  });

  it('should render a Mapview component', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('should validate the postalcode prop', () => {
    const { validator } = wrapper.vm.$options.props.postalCode;

    expect(validator()).toBe(false);
    expect(validator({})).toBe(false);
    expect(validator([])).toBe(false);
    expect(validator(undefined)).toBe(false);
    expect(validator(null)).toBe(false);
    expect(validator(1234)).toBe(false);
    expect(validator('1234A')).toBe(false);
    expect(validator('123AB')).toBe(false);
    expect(validator('1234ABC')).toBe(false);
    expect(validator('123ABC')).toBe(false);

    expect(validator('1234AB')).toBe(true);
    expect(validator('1234ab')).toBe(true);
  });
});
