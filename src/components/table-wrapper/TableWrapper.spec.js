import mount from '@/test-utils';
import TableWrapper from './TableWrapper';

describe('TableWrapper component', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount(TableWrapper, {
      slots: {
        default: 'Dit is de inhoud van het slot',
      },
    });
  });

  it('should render a TableWrapper component', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('should render a TableWrapper with error state', () => {
    wrapper.vm.$store.commit('setError', new Error(404));

    wrapper.vm.$nextTick(() => {
      expect(wrapper.html()).toContain(
        'Er is iets fout gegaan bij het ophalen van de gegevens, probeer het nogmaals.',
      );
    });
  });
});
