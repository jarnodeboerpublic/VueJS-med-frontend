export default {
  id: '123',
  name: 'Pietje',
  lastName: 'Bel',
  addressInclNumber: 'vuurtorenweg 1',
  zipCode: '8011 PK',
  city: 'Zwolle',
};
