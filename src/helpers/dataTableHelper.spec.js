import { customSort } from './dataTableHelper';

const data = [
  {
    id: 3,
    client: {
      name: 'Mark',
    },
    status: 'AANGEBODEN',
  },
  {
    id: 1,
    client: {
      name: 'Jarno',
    },
    status: 'BEZORGD',
  },
  {
    id: 2,
    client: {
      name: 'Leroy',
    },
    status: 'ONDERWEG',
  },
];

describe('CustomSort', () => {
  it('should sort the data', () => {
    expect(customSort(data, ['client.name'], [])).toEqual([
      { client: { name: 'Jarno' }, id: 1, status: 'BEZORGD' },
      { client: { name: 'Leroy' }, id: 2, status: 'ONDERWEG' },
      { client: { name: 'Mark' }, id: 3, status: 'AANGEBODEN' },
    ]);

    expect(customSort(data, ['client.name'], [true])).toEqual([
      { client: { name: 'Mark' }, id: 3, status: 'AANGEBODEN' },
      { client: { name: 'Leroy' }, id: 2, status: 'ONDERWEG' },
      { client: { name: 'Jarno' }, id: 1, status: 'BEZORGD' },
    ]);

    expect(customSort(data, ['id'], [])).toEqual([
      { client: { name: 'Jarno' }, id: 1, status: 'BEZORGD' },
      { client: { name: 'Leroy' }, id: 2, status: 'ONDERWEG' },
      { client: { name: 'Mark' }, id: 3, status: 'AANGEBODEN' },
    ]);

    expect(customSort(data, ['id'], [true])).toEqual([
      { client: { name: 'Mark' }, id: 3, status: 'AANGEBODEN' },
      { client: { name: 'Leroy' }, id: 2, status: 'ONDERWEG' },
      { client: { name: 'Jarno' }, id: 1, status: 'BEZORGD' },
    ]);

    expect(customSort(data, ['status'], [])).toEqual([
      { client: { name: 'Mark' }, id: 3, status: 'AANGEBODEN' },
      { client: { name: 'Leroy' }, id: 2, status: 'ONDERWEG' },
      { client: { name: 'Jarno' }, id: 1, status: 'BEZORGD' },
    ]);

    expect(customSort(data, ['status'], [true])).toEqual([
      { client: { name: 'Jarno' }, id: 1, status: 'BEZORGD' },
      { client: { name: 'Leroy' }, id: 2, status: 'ONDERWEG' },
      { client: { name: 'Mark' }, id: 3, status: 'AANGEBODEN' },
    ]);
  });
});
