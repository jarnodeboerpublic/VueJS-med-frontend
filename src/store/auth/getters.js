export default {
  isAdmin: state => state.role === 'ADMIN',
};
