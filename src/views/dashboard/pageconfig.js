export default {
  data: {
    headers: [
      { text: 'Bestelling', value: 'orderNumber' },
      { text: 'Klantnaam', value: 'client.name' },
      { text: 'Bezorgdatum', value: 'deliveryDateTimeWindow', filterable: false },
      { text: 'Status', value: 'status' },
    ],
    itemsPerPage: 10,
  },
};
