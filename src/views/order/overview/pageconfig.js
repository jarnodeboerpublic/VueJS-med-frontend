export default {
  data: {
    headers: [
      { text: 'Bestelling', value: 'orderNumber' },
      { text: 'Klantnaam', value: 'client.name' },
      { text: 'Adres', value: 'deliveryStreet' },
      { text: 'Plaats', value: 'deliveryResidence' },
      { text: 'Bezorgdatum', value: 'deliveryDateTimeWindow' },
      { text: 'Tijdstip', value: 'timeDifference' },
      { text: 'Status', value: 'status' },
    ],
    itemsPerPage: 5,
  },
};
