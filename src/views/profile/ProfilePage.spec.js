import { shallowMount } from '@vue/test-utils';
import ProfilePage from './ProfilePage.vue';

describe('Profile page', () => {
  it('should render a Profile page', () => {
    const wrapper = shallowMount(ProfilePage);

    expect(wrapper).toMatchSnapshot();
  });
});
