import mount from '@/test-utils';
import OrderOverview from './OrderOverview';

describe('OrderOverview component', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount(OrderOverview, {
      sync: false,
    });
  });

  it('should render an order overview view with data', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('should render an order overview with loading state', () => {
    wrapper.vm.$store.dispatch('setLoading', { root: true });

    wrapper.vm.$nextTick(() => {
      expect(wrapper).toMatchSnapshot();
    });
  });

  it('should render an order overview with error state', () => {
    wrapper.vm.$store.commit('setError', { root: true });

    wrapper.vm.$nextTick(() => {
      expect(wrapper).toMatchSnapshot();
    });
  });

  it('should render an order overview view with empty data state ', () => {
    wrapper.vm.$store.commit('order/setOrders', []);

    wrapper.vm.$nextTick(() => {
      expect(wrapper).toMatchSnapshot();
      expect(wrapper.html()).toContain('Er zijn geen bestellingen beschikbaar');
    });
  });

  it('should hide the toptask when no order coming', () => {
    wrapper.vm.$store.commit('order/setOrders', []);

    wrapper.vm.$nextTick(() => {
      expect(wrapper).not.toContain('Eerstvolgende uitlevering');
    });
  });
});
