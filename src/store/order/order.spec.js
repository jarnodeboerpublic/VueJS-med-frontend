import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import { cloneDeep } from 'lodash';

import { storeConfig } from '../store';
import mockOrders from '../../mocks/order.mock';

Vue.use(Vuex);

jest.mock('axios');

describe('Store order', () => {
  let store;

  beforeEach(() => {
    store = new Vuex.Store(cloneDeep(storeConfig));
  });

  it('should fetch the data', async () => {
    axios.get.mockResolvedValue({
      data: mockOrders,
    });

    await store.dispatch('order/getOrderList');

    expect(store.state.order.orders).toEqual(mockOrders);
    expect(store.state.pending).toEqual(false);
    expect(store.state.error).toBeNull();
  });

  it('should fetch data in devmode', async () => {
    store.state.localDev = true;
    await store.dispatch('order/getOrderList');

    expect(store.state.order.orders).toEqual(mockOrders);
  });

  it('should handle an error in state if call has failed', async () => {
    const error = Error('test');
    axios.get.mockRejectedValue(error);

    await store.dispatch('order/getOrderList');

    expect(store.state.pending).toEqual(false);
    expect(store.state.error).toEqual(error);
  });

  it('should return an empty array when no data available', async () => {
    axios.get.mockResolvedValue({});

    store.commit('order/setOrders', []);
    await store.dispatch('order/getOrderList');

    expect(store.state.order.orders).toEqual([]);
  });

  it('should return a specific order', async () => {
    axios.get.mockResolvedValue({
      data: mockOrders,
    });

    await store.dispatch('order/getOrderList');

    const expectedOrder = store.getters['order/orderById'](mockOrders[0].orderNumber);
    delete expectedOrder.timeDifference;

    expect(expectedOrder).toEqual(mockOrders[0]);
  });

  it('should clear the orders', async () => {
    axios.get.mockResolvedValue({
      data: mockOrders,
    });

    await store.dispatch('order/getOrderList');
    expect(store.state.order.orders).toEqual(mockOrders);

    store.commit('order/clearOrders');
    expect(store.state.order.orders).toEqual([]);
  });

  describe('should return the closest order', () => {
    it('one order status ONDERWEG', () => {
      store.state.order.orders = [
        {
          id: 1,
          status: 'ONDERWEG',
        },
      ];

      const closest = store.getters['order/nextOrder'];
      expect(closest.id).toEqual(1);
    });

    it('one order status BEZORGD', () => {
      store.state.order.orders = [
        {
          id: 1,
          status: 'BEZORGD',
        },
      ];

      const closest = store.getters['order/nextOrder'];
      expect(closest).toEqual(null);
    });

    it('two orders, one BEZORGD', () => {
      store.state.order.orders = [
        {
          id: 1,
          status: 'ONDERWEG',
        },
        {
          id: 2,
          status: 'BEZORGD',
        },
      ];

      const closest = store.getters['order/nextOrder'];
      expect(closest.id).toEqual(1);
    });

    it('two valid orders, different dates', () => {
      store.state.order.orders = [
        {
          id: 1,
          status: 'ONDERWEG',
          deliveryDateTimeWindow: 1512796696,
          deliveryDateTimeWindowEnd: 1512796696,
        },
        {
          id: 2,
          status: 'ONDERWEG',
          deliveryDateTimeWindow: 1572796696,
          deliveryDateTimeWindowEnd: 1572796696,
        },
      ];

      const closest = store.getters['order/nextOrder'];
      expect(closest.id).toEqual(1);
    });

    it('two valid orders, same day, different hours', () => {
      store.state.order.orders = [
        {
          id: 1,
          status: 'ONDERWEG',
          orderDate: 1572796696,
          deliveryDateTimeWindow: 1572796696,
          deliveryDateTimeWindowEnd: 1572796696,
          timeDifference: '12:00 - 14:00',
        },
        {
          id: 2,
          status: 'ONDERWEG',
          orderDate: 1572791696,
          deliveryDateTimeWindow: 1572791696,
          deliveryDateTimeWindowEnd: 1572791696,
          timeDifference: '12:00 - 14:00',
        },
      ];

      const closest = store.getters['order/nextOrder'];
      expect(closest.id).toEqual(2);
    });
  });
});
