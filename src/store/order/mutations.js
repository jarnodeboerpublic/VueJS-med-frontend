export default {
  setOrders: (state, payload) => {
    state.orders = payload;
  },
  clearOrders: state => {
    state.orders = [];
  },
};
