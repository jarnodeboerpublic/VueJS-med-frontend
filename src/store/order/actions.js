import axios from 'axios';
import { fakeOrders } from '@/mocks/backend.mock';

export default {
  getOrderList: async ({ commit, dispatch, rootState }, route) => {
    if (fakeOrders({ commit, rootState })) return;

    dispatch('setLoading', null, { root: true });

    const url = `/api/order${route || ''}`;

    await axios
      .get(url, {
        withCredentials: true,
      })
      .then(res => {
        if (res.data) {
          commit('setOrders', res.data);
        }
      })
      .catch(e => {
        commit('setError', e, { root: true });
      })
      .finally(() => {
        commit('setPending', false, { root: true });
      });
  },
};
