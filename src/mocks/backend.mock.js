import orderMock from '@/mocks/order.mock';

export function fakeLoginAllowed({ commit, rootState }, payload) {
  if (rootState.localDev) {
    if (payload.email.toLowerCase() === 'admin@med.nl' && payload.password === 'admin1234') {
      commit('setLoggedIn', true);
      commit('setRole', 'ADMIN');

      return true;
    }

    if (payload.email.toLowerCase() === 'user@gebruiker.nl' && payload.password === 'user1234') {
      commit('setLoggedIn', true);
      commit('setRole', 'CLIENT');

      return true;
    }
  }
  return false;
}

export function fakeLogoutAllowed({ commit, rootState }) {
  if (rootState.localDev) {
    commit('setLoggedIn', false);
    commit('setRole', null);
    commit('order/clearOrders');

    return true;
  }

  return false;
}

export function fakeOrders({ commit, rootState }) {
  if (rootState.localDev) {
    commit('setOrders', orderMock);
    return true;
  }

  return false;
}
