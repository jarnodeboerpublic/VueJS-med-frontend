import mount from '@/test-utils';
import DashboardPage from '@/views/dashboard/DashboardPage';

describe('DashboardPage', () => {
  it('should render a dashboard page', () => {
    const wrapper = mount(DashboardPage);

    expect(wrapper).toMatchSnapshot();
  });
});
