import axios from 'axios';
import userMock from '@/mocks/user.mock';

export default {
  getUser: async ({ commit, dispatch, rootState }) => {
    if (rootState.localDev) {
      commit('setUser', userMock);
      return;
    }

    dispatch('setLoading', null, { root: true });

    await axios
      .get('/api/user', {
        withCredentials: true,
      })
      .then(res => {
        if (res.data) {
          commit('setUser', res.data);
        }
      })
      .catch(e => {
        commit('setError', e, { root: true });
      })
      .finally(() => {
        commit('setPending', false, { root: true });
      });
  },

  saveUserChanges: async ({ commit, dispatch, rootState }, payload) => {
    if (rootState.localDev) {
      commit('setUser', payload);
      return;
    }

    dispatch('setLoading', null, { root: true });

    await axios
      .put('/api/user', payload, {
        withCredentials: true,
      })
      .then(res => {
        if (res.data) {
          commit('setUser', res.data);
        }
      })
      .catch(e => {
        commit('setError', e, { root: true });
      })
      .finally(() => {
        commit('setPending', false, { root: true });
      });
  },
};
