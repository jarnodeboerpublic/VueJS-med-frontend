import mount from '@/test-utils';
import ContactDetails from './ContactDetails';

// ToDo setup component before testing so more extensive testing can be done
describe('ContactDetails component', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount(ContactDetails);
  });

  it('should render a ContactDetails component', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
