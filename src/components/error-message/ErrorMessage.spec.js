import mount from '@/test-utils';
import ErrorMessage from './ErrorMessage';

describe('ErrorMessage component', () => {
  it('should render an ErrorMessage component', () => {
    const wrapper = mount(ErrorMessage, {
      slots: {
        default: 'Er is iets fout gegaan bij het ophalen van de gegevens, probeer het nogmaals.',
      },
    });

    expect(wrapper).toMatchSnapshot();
  });
});
