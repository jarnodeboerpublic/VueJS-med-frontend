import mount from '@/test-utils';
import OrderDetails from './OrderDetails';
import orderMock from '@/mocks/order.mock';

describe('OrderDetails component', () => {
  it('should render a OrderDetails component', () => {
    const wrapper = mount(OrderDetails, {
      propsData: {
        id: orderMock[0].orderNumber,
      },
    });

    expect(wrapper).toMatchSnapshot();
  });
});
