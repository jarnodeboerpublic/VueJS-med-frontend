import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import { cloneDeep } from 'lodash';

import { storeConfig } from '../store';
import userMock from '../../mocks/user.mock';

Vue.use(Vuex);

jest.mock('axios');

describe('Store user', () => {
  let store;

  beforeEach(() => {
    store = new Vuex.Store(cloneDeep(storeConfig));
  });

  it('should fetch the data', async () => {
    axios.get.mockResolvedValue({
      data: userMock,
    });

    await store.dispatch('user/getUser');

    expect(store.state.user.user).toEqual(userMock);
    expect(store.state.pending).toEqual(false);
    expect(store.state.error).toBeNull();
  });

  it('should get the data with getter', async () => {
    axios.get.mockResolvedValue({
      data: userMock,
    });

    await store.dispatch('user/getUser');
    const gettingUser = store.getters['user/getUserState'];

    expect(store.state.user.user).toEqual(gettingUser);
    expect(store.state.pending).toEqual(false);
    expect(store.state.error).toBeNull();
  });

  it('should fetch data in devmode', async () => {
    store.state.localDev = true;
    await store.dispatch('user/getUser');

    expect(store.state.user.user).toEqual(userMock);
  });

  it('should handle an error in state if call has failed', async () => {
    const error = Error('test');
    axios.get.mockRejectedValue(error);

    await store.dispatch('user/getUser');

    expect(store.state.pending).toEqual(false);
    expect(store.state.error).toEqual(error);
  });

  it('should return an empty object when there is no data available', async () => {
    axios.get.mockResolvedValue({});

    store.commit('user/setUser', {});
    await store.dispatch('user/getUser');

    expect(store.state.user.user).toEqual({});
  });

  it('should save the user', async () => {
    const user = userMock;
    user.lastName = 'Wiegerson';
    axios.put.mockResolvedValue({
      data: user,
    });

    await store.dispatch('user/saveUserChanges', user);

    expect(store.state.user.user).toEqual(user);
  });

  it('should save data in devmode', async () => {
    store.state.localDev = true;
    const user = userMock;
    user.lastName = 'Wiegerson';

    await store.dispatch('user/saveUserChanges', user);

    expect(store.state.user.user).toEqual(user);
  });

  it('should handle an error in state when put request has failed', async () => {
    const error = Error('failed');
    axios.put.mockRejectedValue(error);

    await store.dispatch('user/saveUserChanges', userMock);

    expect(store.state.pending).toEqual(false);
    expect(store.state.error).toEqual(error);
  });
});
