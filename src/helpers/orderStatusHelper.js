const orderStatusHelper = {
  AANGEBODEN: {
    name: 'aangeboden',
    color: 'purple',
    order: 1,
  },
  INBEHANDELING: {
    name: 'in behandeling',
    color: 'blue',
    order: 2,
  },
  BEZORGD: {
    name: 'bezorgd',
    color: 'green',
    order: 4,
  },
  ONDERWEG: {
    name: 'onderweg',
    color: 'blue-grey',
    order: 3,
  },
  GEANNULEERD: {
    name: 'geannuleerd',
    color: 'red',
    order: 5,
  },
};

export default orderStatusHelper;
