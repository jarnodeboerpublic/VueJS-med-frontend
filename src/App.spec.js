import App from './App';
import mount from '@/test-utils';

describe('App component', () => {
  it('should render a default App component', () => {
    const wrapper = mount(App);

    expect(wrapper).toMatchSnapshot();
    expect(wrapper.html()).toContain('Inloggen');
    expect(wrapper.html()).toContain('Contact');
    expect(wrapper.html()).not.toContain('Dashboard');
    expect(wrapper.html()).not.toContain('Bestellingen');
    expect(wrapper.html()).not.toContain('Uitloggen');
  });

  it('should render an App component when logged in', () => {
    const wrapper = mount(App, undefined, {
      modules: {
        auth: {
          state: {
            loggedIn: true,
          },
        },
      },
    });

    expect(wrapper).toMatchSnapshot();
    expect(wrapper.html()).toContain('Bestellingen');
    expect(wrapper.html()).toContain('Uitloggen');
    expect(wrapper.html()).not.toContain('Inloggen');
    expect(wrapper.html()).not.toContain('Dashboard');
  });

  it('should render an App component when loggedIn as admin', () => {
    const wrapper = mount(App, undefined, {
      modules: {
        auth: {
          state: {
            loggedIn: true,
            role: 'ADMIN',
          },
        },
      },
    });

    expect(wrapper).toMatchSnapshot();
    expect(wrapper.html()).toContain('Dashboard');
    expect(wrapper.html()).not.toContain('Contact');
    expect(wrapper.html()).not.toContain('Bestellingen');
  });
});
