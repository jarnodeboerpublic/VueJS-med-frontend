import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';

import auth from './auth';
import order from './order';
import root from './root';
import user from './user';

Vue.use(Vuex);

export const storeConfig = {
  modules: {
    user: {
      namespaced: true,
      ...user,
    },
    order: {
      namespaced: true,
      ...order,
    },
    auth,
  },
  ...root,
};

export default new Vuex.Store({
  ...storeConfig,
  plugins: [createPersistedState({ storage: window.sessionStorage, paths: ['auth', 'order'] })],
});
