const roles = ['ADMIN', 'CLIENT', null];

export default {
  setLoggedIn: (state, payload) => {
    state.loggedIn = payload;
  },
  setRole: (state, payload) => {
    state.role = roles.includes(payload) ? payload : null;
  },
};
